package helloWord;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class HelloWordTest {
	
	PrintStream save_out=System.out;
	final ByteArrayOutputStream out = new ByteArrayOutputStream();
	
	
	@Test
	public void test() {
		System.setOut(new PrintStream(out));
		HelloWord hw = new HelloWord();
		hw.helloWord();
	    assertEquals("hello word\r\n", out.toString());
	    System.setOut(save_out);

		
	}

}
